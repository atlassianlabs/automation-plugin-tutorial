package it.com.atlassianlabs.tutorial;

import com.atlassian.jira.pageobjects.pages.ViewProfilePage;
import org.openqa.selenium.By;

public class CustomViewProfilePage extends ViewProfilePage
{
    public String getTimezone()
    {
        return elementFinder.find(By.id("up-p-timezone-label")).getText();
    }
}
